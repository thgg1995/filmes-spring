package br.com.itau.filmes.services;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.itau.filmes.models.Ator;
import br.com.itau.filmes.models.Filme;

@Service
public class FilmeService {
	
	@Autowired
	AtorService atorService;
	
	ArrayList<Filme> filmes;

	
	public FilmeService() {
		filmes = new ArrayList<>();
		Filme filme1 = new Filme();
		filme1.setTitulo("Os Trapalhões");
		filme1.setAno(1982);
		
		filme1.setId(1);
		Filme filme2 = new Filme();
		filme2.setTitulo("Xuxa");
		filme2.setAno(1990);
		filme2.setId(2);

		filmes.add(filme1);
		filmes.add(filme2);
	}

	public ArrayList<Filme> obterFilmes() {
		for (Filme filme : filmes) {
			ArrayList<Ator> atores = new ArrayList<>();
			atores.add(atorService.obterAtor(filme.getId()));
			filme.setAtores(atores);
		}
		
		return filmes;
	}

	public void inserirFilme(Filme filme) {
		filmes.add(filme);
	}

	public Filme obterFilme(int id) {
		for (Filme filme : filmes) {
			if (filme.getId() == id) {
				return filme;
			}
		}
		return null;
	}
}
