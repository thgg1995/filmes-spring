package br.com.itau.filmes.services;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import br.com.itau.filmes.models.Ator;

@Service
public class AtorService {
	ArrayList<Ator> atores;
	
	public AtorService() {
		atores = new ArrayList<>();
		
		Ator ator1 = new Ator();
		ator1.setNome("Joseph Kimbler");
		ator1.setIdade(34);
		ator1.setNacionalidade("brasileiro");
		ator1.setId(1);
		
		Ator ator2 = new Ator();
		ator2.setNome("Xablau");
		ator2.setIdade(23);
		ator2.setNacionalidade("mexicano");
		ator2.setId(2);
		
		atores.add(ator1);
		atores.add(ator2);
	}
	
	public ArrayList<Ator> obterAtores() {
		return atores;
	}
	
	public void inserirAtor(Ator ator) {
		atores.add(ator);
	}
	
	public Ator obterAtor(int id) {
		for (Ator ator : atores) {
			if (ator.getId() == id) {
				return ator;
			}
		}
		return null;
	}
}
