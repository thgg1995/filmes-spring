package br.com.itau.filmes.controllers;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import br.com.itau.filmes.models.Filme;
import br.com.itau.filmes.services.FilmeService;

@RestController
@RequestMapping("/filme")
public class FilmeController {

	@Autowired
	private FilmeService filmeService;

	@GetMapping
	public ArrayList<Filme> listarFilmes() {

		return filmeService.obterFilmes();
	}

	@GetMapping("/{id}")
	public Filme buscarFilmes(@PathVariable int id) {
		Filme filme = filmeService.obterFilme(id);

		if (filme == null)
			throw new ResponseStatusException(HttpStatus.NOT_FOUND);

		return filme;
	}

	@PostMapping
	@ResponseStatus(code = HttpStatus.CREATED)
	public void criarFilme(@RequestBody Filme filme) {
		filmeService.inserirFilme(filme);
	}
}
