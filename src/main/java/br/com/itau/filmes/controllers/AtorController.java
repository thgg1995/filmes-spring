package br.com.itau.filmes.controllers;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import br.com.itau.filmes.models.Ator;
import br.com.itau.filmes.services.AtorService;

@RestController
@RequestMapping("/ator")
public class AtorController {
	@Autowired
	private AtorService atorService;

	@GetMapping
	public ArrayList<Ator> listarAtores() {

		return atorService.obterAtores();
	}

	@GetMapping("/{id}")
	public Ator buscarFilmes(@PathVariable int id) {
		Ator ator = atorService.obterAtor(id);

		if (ator == null)
			throw new ResponseStatusException(HttpStatus.NOT_FOUND);

		return ator;
	}

	@PostMapping
	@ResponseStatus(code = HttpStatus.CREATED)
	public void criarAtor(@RequestBody Ator ator) {
		atorService.inserirAtor(ator);
	}
}
