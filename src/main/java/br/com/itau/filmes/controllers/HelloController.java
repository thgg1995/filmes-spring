package br.com.itau.filmes.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.itau.filmes.models.Filme;

@RestController
public class HelloController {

	@GetMapping("/hello")
	public Filme dizerHello() {
		Filme filme  = new Filme();
		filme.setTitulo("Mazzaropi");
		filme.setAno(1975);
		
		return filme;
	}
	
	@GetMapping("/again")
	public String helloAgain() {
		return "Hello Darkness My Old Friend AGAIN";
	}
}
